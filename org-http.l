(load "@lib/http.l")
(load "org.l")
(load "picostache.l")

(de readLines (File)
  (filter '((X) X) 
      (make
         (in File
             (until (eof)
                (link (pack (line))))))))

(de passoc (Key Lst) (cadr (seek '((X) (= Key (car X))) Lst)))
(setq lookup passoc) #hack to override picostache to use plists

(allow "*Text")
(setq File "example.org.bak")

(de start() 
   (when *Post
         (out 2 (prin *Text))
         (out File (prin *Text))
         (let Msg (in (list 'git "commit" "-a" "-m" "web update") (line))
            (out 2 (prin Msg))))

  (let (Html (pack (readLines "org-html.html"))
         TemplateTree (parse Html)
         Text (pack (in File (till NIL)))
         Lines (readLines File)
         Org (org-parse Lines 0)
         Model (cons 'org (list Org))
         HeaderTemplate (nth TemplateTree 4))
         
         (setq Template TemplateTree)


         #need to find a more robust way to do this
         (push 'Model '((X) (if View (renderTree (cons 'org (list (list View))) HeaderTemplate))))
         (push 'Model 'orgt)
         (push 'Model (pipe (ht:Prin (pack Text)) (till NIL)))
         (push 'Model 'text)
         (httpHead NIL NIL)
         (prinl (renderTree Model TemplateTree))))

(de go ()
 (server 21000 "!start") )
